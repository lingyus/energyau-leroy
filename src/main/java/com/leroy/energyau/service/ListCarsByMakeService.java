package com.leroy.energyau.service;

import com.leroy.energyau.entity.Car;
import com.leroy.energyau.entity.CarMake;
import com.leroy.energyau.entity.CarShow;

import java.util.Map;
import java.util.Set;

public class ListCarsByMakeService {
    public CarMake listCars(CarShow[] carShows){
        CarMake carMake = new CarMake();
        Map<String, Map<String, Set<String>>> carMakeDetails = carMake.getCarMakeDetails();
        for (CarShow carShow: carShows){
            String showName = carShow.getName();
            for (Car car: carShow.getCars()){
                String make = car.getMake();
                String model = car.getModel();
                if (carMakeDetails.containsKey(make)){
                    if (carMakeDetails.get(make).containsKey(model)){
                        carMake.addShowName(make, model, showName);
                    }else {
                        carMake.addModel(make, model, showName);
                    }
                }else {
                    carMake.addMake(make, model, showName);
                }
            }
        }
        return carMake;
    }
}
