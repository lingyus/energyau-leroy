package com.leroy.energyau.handler;

import com.leroy.energyau.entity.ErrorResponse;
import com.leroy.energyau.exception.BadRequestException;
import com.leroy.energyau.exception.ServerException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<ErrorResponse> handleEmptyBody(BadRequestException exception) {
        return new ResponseEntity(new ErrorResponse(exception.getMsg()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ServerException.class})
    public ResponseEntity<ErrorResponse> handleEmptyBody(ServerException exception) {
        return new ResponseEntity(new ErrorResponse(exception.getMsg()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleBadRequest(Exception exception) {
        return new ResponseEntity(new ErrorResponse(exception.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
    }
}