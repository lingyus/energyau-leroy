package com.leroy.energyau.entity;

import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.util.List;

public class CarShow implements Serializable {
    private final long serialVersionUID = 1L;
    @Nullable
    private String name;
    private List<Car> cars;

    public CarShow(){

    }

    public CarShow(String name, List<Car> cars) {
        this.name = name;
        this.cars = cars;
    }

    public String getName() {
        return name;
    }

    public List<Car> getCars() {
        return cars;
    }

}
