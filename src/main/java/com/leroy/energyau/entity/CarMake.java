package com.leroy.energyau.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CarMake implements Serializable {
    private Map<String, Map<String, Set<String>>> carMakeDetails = new HashMap<>();

    public CarMake(Map<String, Map<String, Set<String>>> carMakeDetails) {
        this.carMakeDetails = carMakeDetails;
    }

    public CarMake(){

    }

    public Map<String, Map<String, Set<String>>> getCarMakeDetails() {
        return carMakeDetails;
    }

    public Map addShowName(String make, String model, String showName){
        if (showName == null || showName.isEmpty()){
            return carMakeDetails;
        }
        this.carMakeDetails.get(make).get(model).add(showName);
        return carMakeDetails;
    }

    public Map addModel(String make, String model, String showName){
        if (model == null || model.isEmpty()){
            return carMakeDetails;
        }
        Set<String> showNames = new HashSet<>();
        if (showName != null && !showName.isEmpty()) {
            showNames.add(showName);
        }
        this.carMakeDetails.get(make).put(model, showNames);
        return carMakeDetails;
    }

    public Map addMake(String make, String model, String showName){
        if (make == null || make.isEmpty() || model == null|| model.isEmpty()){
            return carMakeDetails;
        }
        Set<String> showNames = new HashSet<>();
        if (showName != null && !showName.isEmpty())
        showNames.add(showName);
        Map<String, Set<String>> models = new HashMap<>();
        models.put(model, showNames);
        this.carMakeDetails.put(make, models);
        return carMakeDetails;
    }
}
