package com.leroy.energyau.entity;

import org.springframework.lang.Nullable;

import java.io.Serializable;

public class Car implements Serializable {
    @Nullable
    private String make;
    @Nullable
    private String model;

    public Car(){

    }

    public Car(String make, String model) {
        this.make = make;
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

}
