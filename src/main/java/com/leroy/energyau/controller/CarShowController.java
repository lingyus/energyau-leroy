package com.leroy.energyau.controller;

import com.leroy.energyau.entity.CarMake;
import com.leroy.energyau.entity.CarShow;
import com.leroy.energyau.exception.BadRequestException;
import com.leroy.energyau.exception.ServerException;
import com.leroy.energyau.service.ListCarsByMakeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@RestController
@ResponseBody
public class CarShowController {
    Logger logger = LoggerFactory.getLogger("Log");
    @RequestMapping("/show")
    public Map carShow() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<String> response
                    = restTemplate.getForEntity("http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars", String.class);
            logger.debug(response.toString());
        }catch (Exception e){
            logger.error(String.valueOf(e));
            throw new BadRequestException("Bad request. Please try again");
        }
        try {
            CarShow[] carShows = restTemplate.getForObject("http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars", CarShow[].class);
            ListCarsByMakeService listCarsByMakeService = new ListCarsByMakeService();
            CarMake carMake = listCarsByMakeService.listCars(carShows);
            return carMake.getCarMakeDetails();
        }catch (Exception e){
            logger.error(String.valueOf(e));
            throw new ServerException("The server has some errors temporarily, please wait a second and try again");
        }
    }
}
