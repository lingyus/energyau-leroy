package com.leroy.energyau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergyauApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnergyauApplication.class, args);
	}

}
