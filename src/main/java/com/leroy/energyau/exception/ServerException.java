package com.leroy.energyau.exception;

public class ServerException extends Exception{
    private String msg;

    public ServerException(String msg){
        this.msg = msg;
    }

    public String getMsg() {
        return this.msg;
    }
}
