package com.leroy.energyau.exception;

public class BadRequestException extends Exception{
    private String msg;

    public BadRequestException(String msg){
        this.msg = msg;
    }

    public String getMsg() {
        return this.msg;
    }
}
