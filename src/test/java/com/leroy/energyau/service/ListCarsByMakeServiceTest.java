package com.leroy.energyau.service;

import com.leroy.energyau.entity.Car;
import com.leroy.energyau.entity.CarMake;
import com.leroy.energyau.entity.CarShow;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ListCarsByMakeServiceTest {

    @Test
    public void listCarsTest() throws IOException {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("make1", "model11"));
        cars.add(new Car("make2", "model21"));
        cars.add(new Car("make2", "model22"));
        CarShow carShow1 = new CarShow("show1", cars);
        CarShow carShow2 = new CarShow("show2", cars);
        CarShow[] carShows = new CarShow[]{carShow1, carShow2};
        ListCarsByMakeService listCarsByMakeService = new ListCarsByMakeService();
        CarMake carMake = listCarsByMakeService.listCars(carShows);
        assertThat(carMake.getCarMakeDetails().containsKey("make1"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make1").containsKey("model11"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make1").get("model11").contains("show1"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make1").get("model11").contains("show2"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").containsKey("model21"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").get("model21").contains("show1"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").get("model21").contains("show2"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").containsKey("model22"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").get("model22").contains("show1"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").get("model22").contains("show2"), is(true));
    }

    @Test
    public void listCarsMissingShowNameTest(){
        List<Car> cars1 = new ArrayList<>();
        List<Car> cars2 = new ArrayList<>();
        cars1.add(new Car("make1", "model11"));
        cars1.add(new Car("make2", "model21"));
        cars1.add(new Car("make2", "model22"));
        cars2.add(new Car("make1", "model11"));
        cars2.add(new Car("make3", "model31"));
        CarShow carShow1 = new CarShow("show1", cars1);
        CarShow carShow2 = new CarShow(null, cars2);
        CarShow[] carShows = new CarShow[]{carShow1, carShow2};
        ListCarsByMakeService listCarsByMakeService = new ListCarsByMakeService();
        CarMake carMake = listCarsByMakeService.listCars(carShows);
        assertThat(carMake.getCarMakeDetails().containsKey("make1"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make1").containsKey("model11"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make1").get("model11").contains("show1"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make1").get("model11").size(), is(1));
        assertThat(carMake.getCarMakeDetails().get("make2").containsKey("model21"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").get("model21").contains("show1"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").get("model21").size(), is(1));
        assertThat(carMake.getCarMakeDetails().get("make2").containsKey("model22"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").get("model22").contains("show1"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make2").get("model22").size(), is(1));
        assertThat(carMake.getCarMakeDetails().containsKey("make3"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make3").containsKey("model31"), is(true));
        assertThat(carMake.getCarMakeDetails().get("make3").get("model31").isEmpty(), is(true));
    }


}