###Purpose
This application can produce a list of the models of cars and the show they attended, grouped by make alphabetically, based on data from an existing API (http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars)

###Instruction
To run the program, please download the project to any directory. Then open terminal, change to the project directory. The name of the project is "energyau". So in command line, you can type like this:
````
cd ~/energyau
````

And type the following to run the program
````
./run-app.sh
````

When you see the information like the following,
````
Started EnergyauApplication in 5.688 seconds (JVM running for 9.519)"
````
that means the application is ready

Then, you can open your browser, and type "http://localhost:8080/show" as URL, the result will be displayed on your browser